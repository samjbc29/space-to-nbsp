<?php
class Convertor
{
	/**
	 * Nahrazuje mezery za nedělitelné mezery
	 * Nahrazuje anglické uvozovky za české
	 * z "text" >>> „text“
	 */

	const FORMAT = [
		'HTML' => [
			'NON_BRAKE_SPACE' => '&nbsp;',
			'QUOTATION_START' => '&#8222;', // „
			'QUOTATION_END' => '&#8220;', // “
		],
		'FORMAT_TEX' => [
			'NON_BRAKE_SPACE' => '~',
			'QUOTATION_START' => '\\uv{',
			'QUOTATION_END' => '}',
		],
	];



	/**
	 * Replace function
	 * @param string $text Input
	 * @param string $format Document format
	 *
	 * @return string
	 */
	public function convert(string $text, string $format = 'HTML'): string
	{
		// předložky ve větě
		$pattern = '/\s(k|s|v|z|o|u|i|a|na|pod|před|za|od|do|po|při)\s/';
		$text = preg_replace($pattern, ' ${1}' . self::FORMAT[$format]['NON_BRAKE_SPACE'], $text);

		// předložky na začátku věty
		$pattern = '/(K|S|V|Z|O|U|I|A|Na|Pod|Před|Za|Od|Do|Po|Při)\s/';
		$text = preg_replace($pattern, '${1}' . self::FORMAT[$format]['NON_BRAKE_SPACE'], $text);

		// čísla
		$pattern = '/(?<=\d)\s(?![a-z])\s/';
		$text = preg_replace($pattern, '${1}' . self::FORMAT[$format]['NON_BRAKE_SPACE'], $text);

		// uvozovky
		$pattern = '/(\")(.*?)(\")/';
		$text = preg_replace($pattern, self::FORMAT[$format]['QUOTATION_START'] . '${2}' . self::FORMAT[$format]['QUOTATION_END'], $text);


		return $text;
	}
}

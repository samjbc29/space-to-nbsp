<?php
require './Convertor.php';


class SpacesToNbs
{

	/**
	 * Spuštění aplikace
	 * @param string $text
	 * @param file $file
	 * @param string $format
	 * @param string $output
	 */
	public function run($text, $file = [], $format = 'HTML', $output = 'textarea'): string
	{
		$convertor = new Convertor();

		switch ($format) {
			case 'tex':
				$format = 'TEX';
				break;
			default:
			case 'html':
				$format = 'HTML';
				break;
		}

		if (isset($file['type']) && is_uploaded_file($file['tmp_name']) && $file['type'] == 'text/plain') {
			$text = file_get_contents($file['tmp_name']) ?: '';
		}

		$text = $convertor->convert($text, $format);

		if ($output == 'textarea') {
			return htmlentities($text);
		} else {
			$this->fileHeaders('out.txt');
			echo $text;
			exit;
		}
	}



	/**
	 * Set header for download file
	 * @param string $filename
	 */
	private function fileHeaders($filename): void
	{
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");
	}
}

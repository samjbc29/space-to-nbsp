<?php
require './SpacesToNbsp.php';

$text = '';

if (isset($_POST['output'])) {
	$spacesToNbsp = new SpacesToNbs();

	if ($_POST['output'] == 'textarea') {
		$text = $spacesToNbsp->run($_POST['inputText'], $_FILES['inputFile'], $_POST['format'], $_POST['output']);
	} else {
		$spacesToNbsp->run($_POST['inputText'], $_FILES['inputFile'], $_POST['format'], $_POST['output']);
	}
}
?>

<!DOCTYPE HTML>
<html>

<head>
	<title>Space to nbsp</title>
	<meta charset="utf-8">
</head>

<body>
	<form method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>K převodu:</legend>

			<label for="inputFile">Soubor</label>
			<input type="file" name="inputFile" id="inputFile" accept="text/plain">

			<br><br>

			<label for="inputText">Text</label><br>
			<textarea name="inputText" id="inputText" rows="10" cols="120"><?= $_POST['inputText'] ?? ''; ?></textarea>
		</fieldset>

		<fieldset>
			<legend>Formát:</legend>
			<input type="radio" name="format" id="formatHtml" value="html" checked>
			<label for="formatHtml">HTML</label>

			<br>

			<input type="radio" name="format" id="formatTex" value="text">
			<label for="formatTex">tex</label><br>
		</fieldset>

		<fieldset>
			<legend>Výstup:</legend>
			<input type="radio" name="output" id="outputFile" value="file">
			<label for="outputFile">soubor</label>

			<br>

			<input type="radio" name="output" id="outputText" value="textarea" checked>
			<label for="outputText">textarea</label><br>
		</fieldset>

		<input type="submit" value="převést">
	</form>

	<textarea name="output" rows="10" cols="120"><?= $text; ?></textarea>
</body>

</html>